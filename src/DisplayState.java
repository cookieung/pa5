import java.util.Calendar;

/**
 * State that will Show the time.
 * @author KopZa
 *
 */
public class DisplayState implements State {

	private DigitalClockUI clockUI;
	public final State displayTimeState = new DisplayTimeState();
	public final State displayAlertState = new DisplayAlertState();
	
	/**
	 * Initial state.
	 */
	private State state = displayTimeState;
	
	/**
	 * Construct the display state class
	 * @param clock is the UI of clock that will show.
	 */
	public DisplayState(DigitalClockUI clock){
		clockUI = clock;
	}
	
	@Override
	public void updateTime() {
		state.updateTime();
		
	}

	
	@Override
	public void performSet() {
		state.performSet();
		
	}

	@Override
	public void performPlus() {
		state.performPlus();
		
	}

	@Override
	public void performMinus() {
		state.performMinus();
	}
	
	class DisplayTimeState implements State{

		@Override
		public void updateTime() {
			clockUI.displayTime();
		}

		@Override
		public void performSet() {
			clockUI.setState(clockUI.SETTING_STATE);
		}

		@Override
		public void performPlus() {
			state = displayAlertState;
			
		}

		@Override
		public void performMinus() {
			clockUI.getDigitalClock().toggleAlert();
			clockUI.switchOff();
		}
		
	};
	
	/**
	 * Display State when the alarm is alerted.
	 * @author Salilthip 5710546640.
	 *
	 */
	class DisplayAlertState implements State{
		
		@Override
		public void updateTime() {
			clockUI.displayAlertTime();		
		}

		@Override
		public void performSet() {
			//don't do anything.
		}

		@Override
		public void performPlus() {
			state = displayTimeState;
		}

		@Override
		public void performMinus() {
			//don't do anything.
		}	

	};

	
}