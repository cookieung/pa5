import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * This is a UI of cheap digital clock that can setting the time for ringing.
 * @author Salilthip 5710546640
 *
 */
public class DigitalClockUI extends JFrame {

	private DigiClock clock;
	private State state;
	private Container contents;
	private LayoutManager layout;
	private JButton setting, plus, minus;
	private JPanel panel1, panel2 , panel3;
	private JLabel hour1, hour2, minute1, minute2, sec1, sec2, colon, colon2 , status , on , off;

	public final State SETTING_STATE = new SettingState(this);
	public final State ALERT_STATE = new AlertState(this);
	public final State DISPLAYTIME_STATE = new DisplayState(this);
	
	private Clip clip = importSound();
	
	private Timer show;
	private Timer hide;

	int delay = 500;

	/**
	 * Constructor of the clock
	 * Make the digital number,
	 * Buttons and state.
	 */
	public DigitalClockUI() {
		super("Digital Clock");
		clock = new DigiClock();
		initComponents();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		state = DISPLAYTIME_STATE;
		state.updateTime();
	}

	/**
	 * Change the state.
	 * @param newState is a requirement.
	 */
	public void setState(State newState) {
		this.state = newState;
	}

	/**
	 * Set the time for alarm.
	 * @param field is the value that want to change.
	 * @param time is the new time that want to set.
	 */
	public void setAlert(int field, int time) {
		displayAlertTime();
		clock.setAlert(field, time);
	}


	/**
	 * Update time and check for alarm.
	 */
	ActionListener updateTask = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			clock.update();
			if (clock.check()) {
				setState(ALERT_STATE);
			}
			state.updateTime();
		}
	};
	
	javax.swing.Timer updateTimer = new javax.swing.Timer(delay, updateTask);

	/**
	 * Initialize components in the window.
	 */
	private void initComponents() {
		Class clazz = this.getClass();
		URL url = clazz.getResource("/images/blank.png");
		ImageIcon img = new ImageIcon(url);

		URL url2 = clazz.getResource("/images/colon.gif");
		ImageIcon img2 = new ImageIcon(url2);

		contents = new Container();
		panel1 = new JPanel();
		panel1.setBackground(Color.BLACK);
		panel2 = new JPanel();
		panel2.setBackground(Color.BLACK);
		panel3 = new JPanel();
		panel3.setBackground(Color.gray);
		hour1 = new JLabel(img);
		hour2 = new JLabel(img);
		minute1 = new JLabel(img);
		minute2 = new JLabel(img);
		sec1 = new JLabel(img);
		sec2 = new JLabel(img);
		colon = new JLabel(img2);
		colon2 = new JLabel(img2);
		status = new JLabel("status");
		on = new JLabel("ON");
		off = new JLabel("OFF");
		displayTime();
		layout = new GridLayout(3, 3);
		setting = new JButton("Set");
		setting.setBackground(Color.BLACK);
		setting.setForeground(Color.WHITE);
		plus = new JButton("+");
		plus.setBackground(Color.BLACK);
		plus.setForeground(Color.WHITE);
		minus = new JButton("-");
		minus.setBackground(Color.BLACK);
		minus.setForeground(Color.WHITE);
		status.setForeground(Color.BLACK);
		on.setForeground(Color.BLACK);
		off.setForeground(Color.RED);
		contents.setLayout(layout);
		panel1.add(hour1);
		panel1.add(hour2);
		panel1.add(colon);
		panel1.add(minute1);
		panel1.add(minute2);
		panel1.add(colon2);
		panel1.add(sec1);
		panel1.add(sec2);
		panel2.add(setting);
		panel2.add(plus);
		panel2.add(minus);
		panel3.add(status);
		panel3.add(on);
		panel3.add(off);
		contents.add(panel1);
		contents.add(panel2);
		contents.add(panel3);

		this.add(contents);
		ActionListener listener = new SettingButtonListener();
		setting.addActionListener(listener);
		ActionListener listener2 = new PlusButtonListener();
		plus.addActionListener(listener2);
		ActionListener listener3 = new MinusButtonListener();
		minus.addActionListener(listener3);

		
		show = new Timer(1000, new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				hour1.setVisible(true);
				hour2.setVisible(true);
				minute1.setVisible(true);
				minute2.setVisible(true);
				sec1.setVisible(true);
				sec2.setVisible(true);
				colon.setVisible(true);
				colon2.setVisible(true);
			}
		});
		
		hide = new Timer(1000, new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				hour1.setVisible(false);
				hour2.setVisible(false);
				minute1.setVisible(false);
				minute2.setVisible(false);
				sec1.setVisible(false);
				sec2.setVisible(false);
				colon.setVisible(false);
				colon2.setVisible(false);
			}
		});
		hide.setInitialDelay(500);
		
		updateTimer.start();
	}

	/**
	 * The function of setting button,
	 * it will change state to hour , minute ,second,
	 * and turn on alarm.
	 * @author Salilthip 5710546640.
	 *
	 */
	public class SettingButtonListener implements ActionListener {
		/**
		 * method to perform action when the button is pressed
		 */
		public void actionPerformed(ActionEvent evt) {
			state.performSet();
		}
	}

	/**
	* The function of plus button,
	 * If it is Setting state , we can
	 * plus  the time in hour , minute ,second.
	 * If it is normal state , it will
	 * show alarm time.
	 * @author Salilthip 5710546640.
	 *
	 */
	public class PlusButtonListener implements ActionListener {
		/**
		 * method to perform action when the button is pressed
		 */
		public void actionPerformed(ActionEvent evt) {
			state.performPlus();
		}
	}

	/**
	 * Import picture for show.
	 * @param str is the name of picture.
	 * @return picture that want.
	 */
	public ImageIcon LED(int str) {
		String s = "images/" + str + ".png";
		Class clazz = this.getClass();
		URL alarm = clazz.getResource(s);
		ImageIcon icon = new ImageIcon(alarm);
		return icon;
	}
	
	/**
	 * Import sound of alarm clock.
	 * @return clip of sound.
	 */
	public Clip importSound() {
		String str = "sounds/ring.wav";
		AudioInputStream adi = null;
		Class clazz = this.getClass();
		URL alarm = clazz.getResource(str);
		try {
			adi = AudioSystem.getAudioInputStream(alarm);
		} catch (UnsupportedAudioFileException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Clip sound = null;
		try {
			sound = AudioSystem.getClip();
			sound.open(adi);
		} catch (LineUnavailableException | IOException e1) {
			e1.printStackTrace();
		}
		return sound;
	}

	/**
	* The function of minus button,
	 * If it is Setting state , we can
	 * decrease  the time in hour , minute ,second.
	 * If it is normal state , it will
	 * turn on or turn on alarm.
	 * @author Salilthip 5710546640.
	 *
	 */
	public class MinusButtonListener implements ActionListener {
		/**
		 * method to perform action when the button is pressed
		 */
		public void actionPerformed(ActionEvent evt) {
			state.performMinus();
		}
	}

	/**
	 * For show this class.
	 */
	public void run() {
		this.setVisible(true);
	}

	/**
	 * Show the current time.
	 */
	public void displayTime() {
		hour1.setIcon(LED(clock.getCurrHour() / 10));
		hour2.setIcon(LED(clock.getCurrHour() % 10));
		minute1.setIcon(LED(clock.getCurrMinute() / 10));
		minute2.setIcon(LED(clock.getCurrMinute() % 10));
		sec1.setIcon(LED(clock.getCurrSecond() / 10));
		sec2.setIcon(LED(clock.getCurrSecond() % 10));
	}

	/**
	 * Show the alert Time.
	 */
	public void displayAlertTime() {
		hour1.setIcon(LED(clock.getAlertHour() / 10));
		hour2.setIcon(LED(clock.getAlertHour() % 10));
		minute1.setIcon(LED(clock.getAlertMinute() / 10));
		minute2.setIcon(LED(clock.getAlertMinute() % 10));
		sec1.setIcon(LED(clock.getAlertSecond() / 10));
		sec2.setIcon(LED(clock.getAlertSecond() % 10));
	}
	

	/**
	 * Get the clock.
	 * @return the clock of this class.
	 */
	public DigiClock getDigitalClock() {
		return this.clock;
	}
	
	/**
	 * Get the clip.
	 * @return the clip of alarm sound.
	 */
	public Clip getClip(){
		return clip;
	}
	
	/**
	 * Start Blinking.
	 */
	public void startBlink(){
		show.start();
		hide.start();
	}
	
	/**
	 * Stop Blinking.
	 */
	public void stopBlink(){
		show.stop();
		hide.stop();
		hour1.setVisible(true);
		hour2.setVisible(true);
		minute1.setVisible(true);
		minute2.setVisible(true);
		sec1.setVisible(true);
		sec2.setVisible(true);
		colon.setVisible(true);
		colon2.setVisible(true);
	}
	
	/**
	 * Show the state of setting button.
	 * @param str is the name of state that will show.
	 */
	public void showState(String str){
		status.setForeground(Color.YELLOW);
		status.setText(str);
	}
	
	/**
	 * Show if setting is turned off.
	 */
	public void TurnOffState(){
		status.setForeground(Color.BLACK);
	}
	
	/**
	 * Show ON if alarm is turned on.
	 */
	public void switchOn(){
		on.setForeground(Color.GREEN);
		off.setForeground(Color.BLACK);
	}
	
	/**
	 * Show OFF if alarm is turned on.
	 */
	public void switchOff(){
		on.setForeground(Color.BLACK);
		off.setForeground(Color.RED);
	}
	
	
}
