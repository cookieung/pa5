import javax.sound.sampled.Clip;

/**
 * State when the alarm time.
 * @author Salilthip 5710546640.
 *
 */
public class AlertState implements State {
	
	DigitalClockUI clockUI;
	
	/**
	 * Construct the Alert state class
	 * @param clock is the UI of clock that will show.
	 */
	public AlertState(DigitalClockUI clock) {
		this.clockUI = clock;
		System.out.println("ALert St.");
	}

	@Override
	public void updateTime() {
		if(clockUI.getClip() != null)
			clockUI.getClip().loop(Clip.LOOP_CONTINUOUSLY);
		clockUI.displayAlertTime();
		clockUI.startBlink();
	}

	@Override
	public void performSet() {
		stopAlert();
	}

	@Override
	public void performPlus() {
		stopAlert();
	}

	@Override
	public void performMinus() {
		stopAlert();
	}
	
	/**
	 * Function when turn off the alarm.
	 */
	private void stopAlert(){
		clockUI.getDigitalClock().turnOffAlarm();
		clockUI.setState(clockUI.DISPLAYTIME_STATE);
		clockUI.getClip().stop();
		clockUI.stopBlink();
		clockUI.switchOff();
	}

}
