import java.util.Calendar;


public class SettingState implements State{

	DigitalClockUI clockUI;
	private final State hour = new Hour();
	private final State minute = new Minute();
	private final State second = new Second();
	
	/**
	 * Initial state.
	 */
	State state = hour;

	/**
	 * Construct the setting state class
	 * @param clock is the UI of clock that will show.
	 */
	public SettingState(DigitalClockUI clock){
		this.clockUI = clock;
		System.out.println(clockUI.getClip());
	}

	@Override
	public void updateTime() {
		state.updateTime();
	}

	@Override
	public void performSet() {
		state.performSet();
		
	}

	@Override
	public void performPlus() {
		state.performPlus();		
	}

	@Override
	public void performMinus() {
		state.performMinus();
	}
	
	/**
	 * Hour state
	 * @author Salillthip 5710546640.
	 */
	class Hour implements State{
		
		
		@Override
		public void updateTime() {
			clockUI.displayAlertTime();
			clockUI.showState("HOUR");
		}
		
		@Override
		public void performSet() {
			state = minute;
		}

		@Override
		public void performPlus() {
			clockUI.setAlert(Calendar.HOUR_OF_DAY, clockUI.getDigitalClock().getAlertHour() +1);
			
		}

		@Override
		public void performMinus() {
			clockUI.setAlert(Calendar.HOUR_OF_DAY, clockUI.getDigitalClock().getAlertHour() -1);
			
		}
	}
	
	/**
	 * Minute state
	 * @author Salilthip 5710546640.
	 *
	 */
		class Minute implements State {
		
		@Override
		public void updateTime() {
			clockUI.displayAlertTime();
			clockUI.showState("MINUTE");
		}
		
		@Override
		public void performSet() {
			state = second;
		}
		
		
		@Override
		public void performPlus() {
			clockUI.setAlert(Calendar.MINUTE, clockUI.getDigitalClock().getAlertMinute() +1);
			
		}

		@Override
		public void performMinus() {
			clockUI.setAlert(Calendar.MINUTE, clockUI.getDigitalClock().getAlertMinute() -1);
			

		}
	}
		
		/**
		 * Second State
		 * @author Salilthip 5710546640.
		 *
		 */
		class Second implements State {
			
			@Override
			public void updateTime() {
				clockUI.displayAlertTime();
				clockUI.showState("SECOND");
			}
			
			@Override
			public void performSet() {
				state = hour;
				clockUI.getDigitalClock().turnOnAlarm();
				clockUI.setState(clockUI.DISPLAYTIME_STATE);
				clockUI.switchOn();
				clockUI.TurnOffState();
			}
			

			@Override
			public void performPlus() {
				clockUI.setAlert(Calendar.SECOND, clockUI.getDigitalClock().getAlertSecond() +1);
				
			}

			@Override
			public void performMinus() {
				clockUI.setAlert(Calendar.SECOND, clockUI.getDigitalClock().getAlertSecond() -1);
				
			}
		}
	
}
