public interface State{
	
	/**
	 * Update this state.
	 */
	public void updateTime();
	
	/**
	 * Set this state.
	 */
	public void performSet();
	
	/**
	 *State perform by plus button.
	 */
	public void performPlus();
	
	/**
	 *State perform by minus button.
	 */
	public void performMinus();
	
}