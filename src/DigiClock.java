import java.util.Calendar;

/**
 * The clock class that will update time.
 * @author Salilthip 5710546640.
 *
 */
public class DigiClock{


	private Calendar currentTime;
	private Calendar alertTime;
	private boolean isOn;

	/**
	 * Initial the clock and set time.
	 */
	public DigiClock() {
		initAlertTime();
		currentTime = Calendar.getInstance();		
		isOn = false;
	}
	

	/**
	 * Update the current time.
	 */
	public void update(){
		currentTime.setTimeInMillis( System.currentTimeMillis() );
	}
	
	/**
	 * Turn on alarm.
	 */
	public void turnOnAlarm(){
		isOn = true;
	}
	
	/**
	 * Turn off alarm.
	 */
	public void turnOffAlarm(){
		isOn = false;
	}
	
	/**
	 * Check the current time and alert time if they equal.
	 * @return true if they equal.
	 * 			false if they don't equal.
	 */
	public boolean check(){
		if(alertTime == null || !isOn) return false;
		int h1 = currentTime.get( Calendar.HOUR_OF_DAY );
		int m1 = currentTime.get( Calendar.MINUTE );
		int s1 = currentTime.get( Calendar.SECOND );
		int h2 = alertTime.get( Calendar.HOUR_OF_DAY );
		int m2 = alertTime.get( Calendar.MINUTE );
		int s2 = alertTime.get( Calendar.SECOND );
		return (h1 == h2 && m1 == m2 && s1 == s2 && isOn);
	}
	
	/**
	 * Initial alert time to be 00:00:00.
	 */
	public void initAlertTime(){
		alertTime = Calendar.getInstance();
		alertTime.set(Calendar.HOUR_OF_DAY, 0);
		alertTime.set(Calendar.MINUTE, 0);
		alertTime.set(Calendar.SECOND, 0);
	}

	/**
	 * Set Alert time.
	 * @param field is the original time.
	 * @param value is new time that want.
	 */
	public void setAlert(int field, int value){
		alertTime.set(field, value);
		
	}
	
	/**
	 * Get the current hour.
	 * @return the hour of current time now.
	 */
	public int getCurrHour(){
		return currentTime.get(Calendar.HOUR_OF_DAY);
	}
	
	/**
	 * Get the current minute.
	 * @return the minute of current time now.
	 */
	public int getCurrMinute(){
		return currentTime.get(Calendar.MINUTE);
	}
	
	/**
	 * Get the current second.
	 * @return the second of current time now.
	 */
	public int getCurrSecond(){
		return currentTime.get(Calendar.SECOND);
	}
	
	/**
	 * Get the alert hour.
	 * @return the hour of alert time now.
	 */
	public int getAlertHour(){
		return alertTime.get(Calendar.HOUR_OF_DAY);
	}
	
	/**
	 * Get the alert minute.
	 * @return the minute of alert time now.
	 */
	public int getAlertMinute(){
		return alertTime.get(Calendar.MINUTE);
	}
	
	/**
	 * Get the alert second.
	 * @return the second of alert time now.
	 */
	public int getAlertSecond(){
		return alertTime.get(Calendar.SECOND);
	}
	
	/**
	 * Turn on/off the alarm.
	 */
	public void toggleAlert(){
		if(isOn)
			turnOffAlarm();
		else
			turnOnAlarm();
	}
	
}
	
