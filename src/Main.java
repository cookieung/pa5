/**
 * Application class of the cheap digital clock.
 * @author Salilthip 5710546640.
 *
 */
public class Main {

	public static void main(String[] args) {
		DigitalClockUI c = new DigitalClockUI();
		c.run();
	}
}
